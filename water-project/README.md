Filtering Water
===============

Water analyses
--------------

Water filtering / life-support system
Short description:
I plan to filter water collected in nature, in order to make it drinkable. This can be very useful when you don't have tap water, for instance when you're trekking.
Actions and deliverables:
Collect samples from different water sources
Tapwater, water from the aquarium, water from Utterslev Mose, water from a puddle near the road, water from a puddle away from busy streets.
Analyze the water(bacteria?, nitrates?, chemicals?)


Build one or more filters and/or use off-the-shelf filters
Filter the water and analyze it again
Summarize the findings in a report/presentation/video/…

|                     | Ammonium | Total hardness | Nitrat | Nitrit | Phosphate | pH |
| ------------------- | -------- | -------------- | ------ | ------ | --------- | -- |
| Tapwater            |          | 0              | 0      |        |           | 7  |
| Aquarium            | 0,2      |                | 90+    |        |           | 7  |
| Clean aquarium      | 0,1      |                | 8      |        |           |    |
| Puddle near road    |          | 3              | 3      |        |           | 7  |
| Puddle far from road|          | 0,5            | 0,5    |        |           | 6  |
| Utterslev Mose      | 0,2      | 1              | 1      |        |           | 6-7|
