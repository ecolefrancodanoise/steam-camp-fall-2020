Getting started with RSA.
=========================
install the relevant packages:
```
sudo apt-get install libgmp-dev libgmpxx4ldbl qrencode zbar-tools
```
Compile the encode program:
```
g++ encode.cpp -lgmp -lgmpxx -o encode
```
Assuming the following 64-bit keys:
```
n: bPD0bwGqiUD
e: D5YNpl
d: Z8nfzJZhgdh
```
encrypt the string *donaldduck*:
```
echo donaldduck | ./encode bPD0bwGqiUD D5YNpl
```
Which gives the encoded string *6AcOnEt9LlQ*

Note: (*n*, *e*) are the public key and (*n*, *d*) are the private key.

Then decoding it with:
```
echo 6AcOnEt9LlQ | ./encode bPD0bwGqiUD Z8nfzJZhgdh
```
Which gives the decoded string *donaldduck*

Signing Messages
================

Signing the message *Tillykke* with the private key *Z8nfzJZhgdh*
```
echo Tillykke | ./encode bPD0bwGqiUD Z8nfzJZhgdh
```
Which gives *PWwpqYZFCb9*
Pipe thus into qrencode
```
echo Tillykke | ./encode bPD0bwGqiUD Z8nfzJZhgdh | qrencode -o tillykke.png
```
Generates a QR code in the file tillykke.png

![Tillykke](tillykke.png)

which can be scanned using zbarcam and decoded
```
zbarcam --raw | ./encode bPD0bwGqiUD D5YNpl
```
Generating keys
================
First compile the program:
```
g++ rsa-key-generator.cpp -lgmp -lgmpxx -o rsa-key-generator
```
Then generate the keys with:
```
./rsa-key-generator 100
```
Which will give you the keys *n*,*e* and *d* (*n* being 100 bits)


A simple example explaining RSA
===============================
Assuming the keys:
```
n = p * q = 3 * 7 = 21
d = 11
e such as d * e = 1 mod (p -1)(q -1) = 23
```
Encoding the message 3 as C:
```
C = 3^e mod n = 3^23 mod 21 = 12
```
which gives the encoded message C = 12.
Then decode the encoded message C:
```
m = C^d mod n = 12^11 mod 21 = 3
```

For further reading sea the [RSA paper](http://people.csail.mit.edu/rivest/Rsapaper.pdf)
